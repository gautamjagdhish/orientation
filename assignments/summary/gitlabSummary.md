# Gitlab Summary
## What is Gitlab ?
GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc

## Features :
Gitlab defines 10 stages in the Dev-Ops lifecycle as :
* Manage
* Plan    
* Create    
* Verify    
* Package    
* Secure    
* Release    
* Configure    
* Monitor    
* Defend  

Gitlab provides various feautures for the above stages to help the developer smoothly manage the repository