# Git Summary
## What is Version Control ?
Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later.
It primarily allows users to : 
* Revert selected files back to a previous state
* Revert the entire project back to a previous state
* Compare changes over time
* See who last modified something that might be causing a problem
* Who introduced an issue and when
Generally, a Version Control System(VCS) provides these features for very little overhead.

## What is Git ?
Git is a open source VCS. It has 3 states in which the files can reside in :
* **Modified** means that you have changed the file but have not committed it to your database yet.
* **Staged** means that you have marked a modified file in its current version to go into your next commit snapshot.
* **Committed** means that the data is safely stored in your local database.

![Git Stages Image](gitWorking.png)

